import 'package:flutter/material.dart';

// class MyBody extends StatelessWidget {
//   // возвращает объект виджет
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         Text('Hello Andrey!'),
//         FlatButton(
//           onPressed: () {},
//           child: Text('Открыть сайт'),
//           color: Colors.red,
//           textColor: Colors.white,
//         ),
//       ],
//     );
//   }
// }

// I. Статичный список
// List<Widget> myList = [
//   Text('Line 1'),
//   Divider(),
//   Text('Line 2'),
//   Divider(),
//   Text('Line 3'),
//   Divider(),
//   Text('Line 4'),
// ];

// class MyBody extends StatelessWidget {
//   // возвращает объект виджет
//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       children: myList,
//     );
//   }
// }

// II. Динамический список Получение по одному сообщению
// class MyBody extends StatelessWidget {
//   // возвращает объект виджет
//   @override
//   Widget build(BuildContext context) {
//     return ListView.builder(itemBuilder: (context, index) {
//       return Text('Строка №$index');
//     });
//   }
// }

// III. Получение сообщений По несколько за раз
class MyList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyListState();
}

class MyListState extends State<MyList> {
  List<String> _array = [];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(itemBuilder: (context, i) {
      print('num $i : нечетное = ${i.isOdd}');

      if (i.isOdd) return Divider();

      final int index = i ~/ 2;

      if (index >= _array.length) {
        print('index $index');
        print('length ${_array.length}');

        _array.addAll([
          '${index + 1}',
          '${index + 2}',
          '${index + 3}',
        ]);
      }

      return ListTile(
        title: Text('$i'),
      );
    });
  }
}

class MyBody extends StatelessWidget {
  // возвращает объект виджет
  @override
  Widget build(BuildContext context) {
    return ListView.builder(itemBuilder: (context, index) {
      return Text('Cтрока №${index + 1}');
    });
  }
}

class MyBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.cleaning_services),
          label: 'Очистка',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.camera_alt),
          label: 'Камера',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.photo_library),
          label: 'Фото',
        ),
      ],
    );
  }
}

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.home),
            tooltip: 'Домой',
            onPressed: () => {},
          ),
          title: Text('Price'),
          centerTitle: true,
          // тень, z-координата
          elevation: 4.0,
          actions: [
            IconButton(
              icon: Icon(Icons.menu),
              tooltip: 'Меню',
              onPressed: () => {},
            ),
          ],
        ),
        body: MyBody(),
        bottomNavigationBar: MyBottom(),
      ),
    ),
  );
}
